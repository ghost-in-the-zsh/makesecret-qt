# makesecret-qt

A program to generate strong passwords and passphrases, analyze them, and even securely verify if they are known to have been compromised in past breaches based on available datasets. The latter is done ***without ever revealing the secret*** being verified in the first place. This one is a PyQt5-based graphical front-end for the [makesecret](https://gitlab.com/ghost-in-the-zsh/makesecret) program.


## Requirements

* Python v3.6.5 or later
* Python `makesecretlib` module
* A GNU+Linux shell or Windows command prompt (to launch the program only)

The `makesecretlib` module dependency is automatically installed when installing this program using PIP.


## Installation

You need to use the Python Package Installer. The command syntax may be different depending on the specific platform.


### GNU+Linux

Removing current installations (if any):
```bash
$ pip3 uninstall -y makesecret-qt
```

To install, run this command:
```bash
$ pip3 install https://gitlab.com/ghost-in-the-zsh/makesecret-qt/-/archive/master/makesecret-qt-master.zip
```


### Windows

The syntax for Windows may vary and often depends on whether you have the Python interpreter in your `PATH` or not. Generally, the commands used in the GNU+Linux section are expected to work if you change `pip3 install` to `py -m pip install`. For example:

Removing current installation (if any):
```bash
> py -m pip uninstall -y makesecret-qt
```

Then, to install:
```bash
> py -m pip install https://gitlab.com/ghost-in-the-zsh/makesecret-qt/-/archive/master/makesecret-qt-master.zip
```


## Launching the Program

Just call it from the terminal or command prompt.


### GNU+Linux

```
$ makesecret-qt
```


### Windows

```
> makesecret-qt.exe
```


## Screenshots

#### Passphrases Tab

Tab with options to generate passphrases.

![Passphrases Tab](screenshots/passphrases-tab.png)

#### Passwords Tab

Tab with options to generate passwords.

![Passwords Tab](screenshots/passwords-tab.png)

#### Breach Verification Tab

Check if a secret, or any other text, is known to have been exposed before or not.

![Breach Verification Tab](screenshots/breach-verification-tab.png)

#### Generated Result Window

A generated password or passphrase result with strength and breach analysis options selected.

![Passphrase Generation Result](screenshots/passphrase-generation-result.png)

#### Breach Verification Result

Checking a known weak password, that no one should be using, for known breaches. When the program detects that a secret is known to have been exposed, it blocks the user from being able to copy/use the value, as well as drawing attention to the relevant widgets in red.

![Breach Verification Result](screenshots/breach-verification-result.png)

#### About Window

Some information about the program.

![About Dialog Window](screenshots/about-dialog.png)


## Licensing

This project is [Free Software](https://www.gnu.org/philosophy/free-sw.html) because it respects and protects your freedoms. For a quick summary of what your rights and responsibilities are, you should see [this article](https://tldrlegal.com/license/gnu-general-public-license-v3-(gpl-3)). For the full text, you can see the [license](LICENSE).
