import sys

from makesecret_qt.models import MakeSecretQtApp


def main():
    app = MakeSecretQtApp()
    return app.exec()


if __name__ == '__main__':
    sys.exit(main())
