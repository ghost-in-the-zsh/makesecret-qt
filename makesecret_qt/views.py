from pkg_resources import resource_filename

from PyQt5.Qt import QMainWindow, QStyle, QDialog
from PyQt5.QtCore import Qt, pyqtSlot
from PyQt5.uic import loadUi

from makesecretlib.models import \
    Secret, \
    SecretStrengthMeta, \
    SecretBreachMeta


class MainWindow(QMainWindow):

    def __init__(self, app):
        super().__init__()
        filepath = resource_filename('makesecret_qt', 'ui/mainwindow.ui')
        loadUi(filepath, self)

        # centralize window
        self.setGeometry(QStyle.alignedRect(
            Qt.LeftToRight,
            Qt.AlignCenter,
            self.size(),
            app.desktop().availableGeometry()
        ))


class AboutDialog(QDialog):

    def __init__(self):
        super().__init__()
        filepath = resource_filename('makesecret_qt', 'ui/aboutdialog.ui')
        loadUi(filepath, self)


class ResultDialog(QDialog):

    def __init__(
        self, *,
        secret: Secret,
        strength_meta: SecretStrengthMeta=None,
        breach_meta: SecretBreachMeta=None
    ):
        super().__init__()
        filepath = resource_filename('makesecret_qt', 'ui/resultdialog.ui')
        loadUi(filepath, self)

        self._populate_fields(secret, strength_meta, breach_meta)
        self.close_button.clicked.connect(self.close)
        self.clipboard_copy_button.clicked.connect(self._on_copy_button_clicked)

        self._check_secret_exposure(breach_meta)
        self.adjustSize()

    def _populate_fields(
        self,
        secret: Secret,
        strength_meta: SecretStrengthMeta,
        breach_meta: SecretBreachMeta
    ):
        self.secret_result_line.setText(secret.result)

        self._populate_strength(strength_meta)
        self._populate_breach(breach_meta)

    def _populate_strength(self, strength_meta: SecretStrengthMeta):
        if not strength_meta:
            self.strength_groupbox.hide()
            return

        self._populate_entropy(strength_meta)
        self._populate_tolerance(strength_meta)

    def _populate_entropy(self, strength_meta: SecretStrengthMeta):
        p = strength_meta.precision
        e = 2
        entropy_base = self.selection_pool_label.text().format(strength_meta.entropy_base, p=p)
        entropy_bits_choice = self.entropy_choice_label.text().format(strength_meta.entropy_bits, p=e)
        entropy_bits_total = self.entropy_overall_label.text().format(strength_meta.entropy_bits_total, p=e)
        entropy_bits_effective = self.entropy_effective_label.text().format(strength_meta.entropy_bits_effective, p=e)

        self.selection_pool_label.setText(entropy_base)
        self.entropy_choice_label.setText(entropy_bits_choice)
        self.entropy_overall_label.setText(entropy_bits_total)
        self.entropy_effective_label.setText(entropy_bits_effective)

    def _populate_tolerance(self, strength_meta: SecretStrengthMeta):
        p = strength_meta.precision
        attacks_per_second = self.attempts_per_second_label.text().format(strength_meta.attacks_per_second, p=p)
        crack_total_count = self.total_avg_attempts_label.text().format(strength_meta.crack_total_count, p=p)
        break_seconds = self.break_seconds_label.text().format(strength_meta.crack_secs, p=p)
        break_hours = self.break_hours_label.text().format(strength_meta.crack_hours, p=p)
        break_days = self.break_days_label.text().format(strength_meta.crack_days, p=p)
        break_weeks = self.break_weeks_label.text().format(strength_meta.crack_weeks, p=p)
        break_years = self.break_years_label.text().format(strength_meta.crack_years, p=p)
        break_decades = self.break_decades_label.text().format(strength_meta.crack_decades, p=p)
        break_centuries = self.break_centuries_label.text().format(strength_meta.crack_centuries, p=p)
        break_millenia = self.break_millenia_label.text().format(strength_meta.crack_millenia, p=p)
        break_millions = self.break_millions_label.text().format(strength_meta.crack_millions, p=p)
        break_billions = self.break_billions_label.text().format(strength_meta.crack_billions, p=p)
        acceptable = strength_meta.entropy_bits_effective >= strength_meta.entropy_bits_minimum

        self.attempts_per_second_label.setText(attacks_per_second)
        self.total_avg_attempts_label.setText(crack_total_count)
        self.break_seconds_label.setText(break_seconds)
        self.break_hours_label.setText(break_hours)
        self.break_days_label.setText(break_days)
        self.break_weeks_label.setText(break_weeks)
        self.break_years_label.setText(break_years)
        self.break_decades_label.setText(break_decades)
        self.break_centuries_label.setText(break_centuries)
        self.break_millenia_label.setText(break_millenia)
        self.break_millions_label.setText(break_millions)
        self.break_billions_label.setText(break_billions)

        css = 'QLabel { color: rgb(0, 255, 0); }' if acceptable else 'QLabel { color: rgb(255, 0, 0); }'
        self.acceptable_label.setStyleSheet(css)
        self.acceptable_label_parent.setStyleSheet(css)
        self.acceptable_label.setText('Yes' if acceptable else 'No')

    def _populate_breach(self, breach_meta: SecretBreachMeta):
        if not breach_meta:
            self.breach_groupbox.hide()
            return

        candidate_count = self.api_results_count_label.text().format(breach_meta.candidates_found)
        breach_count = self.known_breaches_label.text().format(breach_meta.known_breaches_count)

        self.hash_full_label.setText(breach_meta.sha1hash)
        self.hash_prefix_label.setText(breach_meta.sha1hash[:5])
        self.hash_suffix_label.setText(breach_meta.sha1hash[5:])
        self.api_request_label.setText(
            f'<a href="{breach_meta.request_url}">{breach_meta.request_url}</a>'
        )
        self.api_results_count_label.setText(candidate_count)
        self.known_breaches_label.setText(breach_count)

    @pyqtSlot()
    def _on_copy_button_clicked(self):
        self.secret_result_line.selectAll()
        self.secret_result_line.copy()

    def _check_secret_exposure(self, breach_meta: SecretBreachMeta):
        if not breach_meta or breach_meta.known_breaches_count == 0:
            return

        # don't want the user blindly copy-pasting unsafe secret
        self.clipboard_copy_button.setEnabled(False)
        self.secret_result_line.setEnabled(False)

        # set some widgets to red, to draw attention
        self.clipboard_copy_button.setStyleSheet(
            'QPushButton { color: rgb(255, 0, 0); }'
        )
        self.secret_result_line.setStyleSheet(
            'QLineEdit { color: rgb(255, 0, 0); }'
        )

        label_css = 'QLabel { color: rgb(255, 0, 0); }'
        self.secret_result_label.setStyleSheet(label_css)
        self.known_breaches_label.setStyleSheet(label_css)
        self.known_breaches_label_parent.setStyleSheet(label_css)
        self.acceptable_label.setStyleSheet(label_css)
        self.acceptable_label_parent.setStyleSheet(label_css)

        # set this to No regardless of what it had, for consistency
        self.acceptable_label.setText('No')
