import sys
import os

from pkg_resources import resource_filename

from PyQt5.Qt import QApplication
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QMessageBox, QFileDialog

from makesecretlib.settings import \
    ATTEMPTS_PER_SECOND, \
    MINIMUM_ENTROPY_BITS, \
    DECIMAL_PRECISION, \
    PASSPHRASE_WORD_COUNT, \
    PASSWORD_CHAR_COUNT
from makesecretlib.providers import \
    PasswordProvider, \
    PassphraseProvider
from makesecretlib.analyzers import \
    StrengthAnalyzer, \
    BreachAnalyzer
from makesecretlib.models import \
    Secret, \
    SecretStrengthMeta, \
    SecretBreachMeta

from makesecret_qt.settings import VERSION
from makesecret_qt.views import \
    MainWindow, \
    AboutDialog, \
    ResultDialog


class MakeSecretQtApp(QApplication):

    def __init__(self):
        super().__init__(sys.argv)
        self._win = MainWindow(self)
        self._apply_settings(self._win)
        self._connect_signals(self._win)
        self._win.show()

    def _connect_signals(self, win: MainWindow):
        win.quit_action.triggered.connect(win.close)
        win.about_action.triggered.connect(self._on_show_about_dialog)
        win.quit_button.clicked.connect(win.close)
        win.words_list_path_edit.textChanged.connect(self._on_words_list_path_edit_text_changed)
        win.phrase_generate_button.clicked.connect(self._on_generate_passphrase_button_clicked)
        win.words_list_file_button.clicked.connect(self._on_words_file_path_button_clicked)
        win.password_generate_button.clicked.connect(self._on_generate_password_button_clicked)
        win.breach_secret_edit.textChanged.connect(self._on_breach_secret_edit_text_changed)
        win.breach_verify_button.clicked.connect(self._on_verify_breach_button_clicked)

    @pyqtSlot()
    def _on_generate_passphrase_button_clicked(self):
        provider = PassphraseProvider(
            wordslist_file=self._win.words_list_path_edit.text(),
            user_choice_cnt=self._win.phrase_words_count_spinbox.value(),
            capitalize_words=self._win.phrase_capitalize_words_checkbox.isChecked(),
            with_numbers=self._win.phrase_include_digits_checkbox.isChecked()
        )
        try:
            self._analyze_if_requested(
                provider.generate(),
                self._win.phrase_words_count_spinbox.value(),
                self._win.phrase_analyze_checkbox.isChecked(),
                self._win.phrase_verify_breaches_checkbox.isChecked()
            )
        except FileNotFoundError as e:
            QMessageBox.critical(
                self._win,
                'Error: File Not Found',
                f'The words list file was not found. Reason:{os.linesep}{repr(e)}',
                QMessageBox.Ok,
                QMessageBox.Ok
            )
        except IOError as e:
            QMessageBox.critical(
                self._win,
                'Error: I/O Error',
                f'The words list file could not be read. Reason:{os.linesep}{repr(e)}',
                QMessageBox.Ok,
                QMessageBox.Ok
            )
        except Exception as e:
            QMessageBox.critical(
                self._win,
                'Error: Derp!',
                f'Something bad happened. I didn\'t think it would, but it did. What:{os.linesep}{repr(e)}',
                QMessageBox.Ok,
                QMessageBox.Ok
            )

    @pyqtSlot(str)
    def _on_words_list_path_edit_text_changed(self, path_text: str):
        self._win.phrase_generate_button.setEnabled(len(path_text) > 0)

    @pyqtSlot(str)
    def _on_breach_secret_edit_text_changed(self, suspect_text: str):
        self._win.breach_verify_button.setEnabled(len(suspect_text) > 0)

    @pyqtSlot()
    def _on_generate_password_button_clicked(self):
        provider = PasswordProvider(
            user_choice_cnt=self._win.password_char_count_spinbox.value(),
            with_special=self._win.password_include_special_checkbox.isChecked()
        )
        self._analyze_if_requested(
            provider.generate(),
            self._win.password_char_count_spinbox.value(),
            self._win.password_analyze_checkbox.isChecked(),
            self._win.password_verify_breaches_checkbox.isChecked()
        )

    @pyqtSlot()
    def _on_verify_breach_button_clicked(self):
        secret = Secret(self._win.breach_secret_edit.text(), 0)
        self._analyze_if_requested(secret, 0, False, True)

    def _analyze_if_requested(self, secret, choices_count, wants_strength, wants_breach):
        strength = None
        breach = None

        if wants_strength:
            strength = StrengthAnalyzer().analyze(
                secret=secret,
                user_choices_count=choices_count,
                #
                # FIXME: Add some UI element (QDialog?) to allow user-config for these values
                #
                attacks_per_second=ATTEMPTS_PER_SECOND,
                entropy_bits_minimum=MINIMUM_ENTROPY_BITS,
                precision=0     # keeps UI cleaner-looking
            )
        if wants_breach:
            try:
                breach = BreachAnalyzer().analyze(secret=secret)
            except RuntimeError as e:
                breach = None
                QMessageBox.critical(
                    self._win,
                    'Error: Breach Analysis Failed',
                    f'Breach analysis could not be performed. Reason:{os.linesep}{repr(e)}',
                    QMessageBox.Ok,
                    QMessageBox.Ok
                )

        self._show_result_dialog(secret=secret, strength_meta=strength, breach_meta=breach)

    @pyqtSlot()
    def _on_words_file_path_button_clicked(self):
        # returns the filename and the filter str that made it available,
        # which we don't care about
        filename, _ = QFileDialog.getOpenFileName(
            self._win,
            'Passphrase Words List File',
            '',
            'All Files (*);;Text Files (*.txt)'
        )
        # when the user chooses cancel, we get an empty string
        # and don't want to change the current selection
        if filename:
            self._win.words_list_path_edit.setText(filename)

    @pyqtSlot()
    def _on_show_about_dialog(self):
        diag = AboutDialog()
        text = diag.summary_label.text().format(VERSION)
        diag.summary_label.setText(text)
        diag.show()
        diag.exec()

    def _show_result_dialog(self, *,
        secret: Secret,
        strength_meta: SecretStrengthMeta,
        breach_meta: SecretBreachMeta
    ):
        diag = ResultDialog(secret=secret, strength_meta=strength_meta, breach_meta=breach_meta)
        diag.show()
        diag.exec()

    def _apply_settings(self, win: MainWindow):
        win.phrase_words_count_spinbox.setValue(PASSPHRASE_WORD_COUNT)
        win.words_list_path_edit.setText(resource_filename('makesecret_qt', 'data/eff_wordlist.db'))

        win.password_char_count_spinbox.setValue(PASSWORD_CHAR_COUNT)
